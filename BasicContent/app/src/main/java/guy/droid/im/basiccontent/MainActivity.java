package guy.droid.im.basiccontent;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.Browser;
import android.provider.MediaStore;
import android.provider.CallLog;
import android.provider.MediaStore.*;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView result;
    String sender = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        result = (TextView) findViewById(R.id.result);

    }

    @SuppressWarnings("PointlessBooleanExpression")
    public void Call(View view) {
        Toast.makeText(this, "Get all Call logs", Toast.LENGTH_SHORT).show();
        String[] projection = new String[]{CallLog.Calls.DATE, CallLog.Calls.NUMBER, CallLog.Calls.DURATION,CallLog.Calls.GEOCODED_LOCATION,CallLog.Calls.PHONE_ACCOUNT_ID
        ,CallLog.Calls.POST_DIAL_DIGITS,CallLog.Calls.TYPE,CallLog.Calls.CACHED_NAME};
        result.setText("");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Cursor c = getContentResolver().query(CallLog.Calls.CONTENT_URI, projection, null, null, null);

        assert c != null;

        c.moveToFirst();

        while (!c.isAfterLast()) {
            String phoneNumber = CallLog.Calls.NUMBER;
            String date =  CallLog.Calls.DATE;
            String geolocation =  CallLog.Calls.GEOCODED_LOCATION;
            String phoneaccountid =  CallLog.Calls.PHONE_ACCOUNT_ID;
            String dialledDigits =  CallLog.Calls.POST_DIAL_DIGITS;
            String type =  CallLog.Calls.TYPE;
            String name =  CallLog.Calls.CACHED_NAME;
            int phoneIndex = c.getColumnIndex(phoneNumber);
            int dateIndex = c.getColumnIndex(date);
            int geoIndex = c.getColumnIndex(geolocation);
            int ACCOUNTIndex = c.getColumnIndex(phoneaccountid);
            int postalIndex = c.getColumnIndex(dialledDigits);
            int typeIndex = c.getColumnIndex(type);
            int nameIndex = c.getColumnIndex(name);

            result.append(c.getString(nameIndex)+"-"+c.getString(typeIndex)+"\n");
            sender+=c.getString(nameIndex)+"\n";
            c.moveToNext();
        }
        c.close();


    }

    public void accessMediaStore(View view) {
        Toast.makeText(this, "Show all Media Objects!", Toast.LENGTH_SHORT).show();
        String[] projection = {
                MediaStore.MediaColumns.DISPLAY_NAME,
                MediaStore.MediaColumns.DATE_ADDED,
                MediaStore.MediaColumns.MIME_TYPE
        };
        CursorLoader loader = new CursorLoader(this, Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, null);
        Cursor c = loader.loadInBackground();
        c.moveToFirst();
        result.setText("");

        while (!c.isAfterLast()) {

            String mediaName = MediaStore.MediaColumns.DISPLAY_NAME;
            int nameIndex = c.getColumnIndex(mediaName);
            result.append(c.getString(nameIndex)+"\n");
            sender+=c.getString(nameIndex)+"\n";
            c.moveToNext();
        }

        c.close();

    }


    public void CustomContent(View view)
    {

        Intent intent = new Intent();
        intent.putExtra("PACKED_VALUE", sender);
        setResult(RESULT_OK, intent);
        finish();
    }

}
