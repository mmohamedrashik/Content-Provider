package guy.droid.im.contentresolver;

import android.content.ActivityNotFoundException;
import android.content.ContentProvider;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_CODE = 5005;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView)findViewById(R.id.displayer);
        try {
            Intent intent = new Intent("guy.droid.im.basiccontent.BASICAct");
            startActivityForResult(intent, REQUEST_CODE);
        } catch (ActivityNotFoundException ex) {
            ex.printStackTrace();
            textView.setText(ex.toString());
            Log.e("Main", "Second application is not installed!");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String intentData = data.getStringExtra("PACKED_VALUE");
                textView.setText(intentData);
            } else {
                textView.setText("User press back at Second Activity");
            }
        }
    }
}
